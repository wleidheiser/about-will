# About - Will Leidheiser

**This was inspired and adapted from resources by the [Autism Education Trust](https://www.autismeducationtrust.org.uk/)**

This issue can be used by team members and managers to build a profile to enable more personalization in the team member & manager relationship. This is particularly important for neurodiverse team members both diagnosed and undiagnosed, as they may have unique needs and accomodations that need to be made to ensure they are successful at GitLab. By focusing on styles rather than diagnosis, it ensures that everyone has the opportunity to express individual needs without disclosing their neurodiversity if they do not wish too. 

#### Communication Style 

- [x] I prefer to communicate verbally
- [ ] I prefer to communicate through written communications 
- [x] I do not communicate well in large groups
- [ ] I can communicate well in large groups
- [x] I can struggle with traditional social cues such as eye contact 
- [ ] I can struggle with talking about non-work related topics whilst at work
- [x] Other: I have difficulty picking up tone through written communciation (for example: sarcasm, frustration, etc.)

#### Working Style

- [ ] I benefit from work tasks being written or backed up with written communication
- [x] I prefer visual information such as videos, charts, graphics and diagrams 
- [x] I prefer to record zoom meetings so I can review them later
- [ ] I can handle multiple questions or instructions put to me at one time 
- [x] I prefer not to have multiple questions or instructions put to me at one time 
- [x] I tend to need time to process information before providing answers
- [x] I prefer to have information or questions prior to meetings or discussions
- [ ] I can struggle with deadlines, I appreciate more communication on tasks and deadlines 
- [x] I don't perform well with constant change, advanced notice to significant changes is preferred
- [x] I work best when I can concentrate on a small number of tasks at one time 
- [ ] Other: (Insert as many of your own statements as you see fit)

#### Working Style Summary 

I prefer a work environment where I can structure my day around the tasks that need to get done. I can manage multiple tasks as long as I block off enough time throughout my day/week to get them complete. Otherwise, it is easy for be to get overloaded and struggle to catch up on my backlog.

<!-- **Example:** I prefer a work environment that is structured, which leans heavily into asynchronous communication. I can manage multiple tasks well but can find drastic changes stressful, which can lead to unwanted stress responses. 

**Example:** I prefer a work environment with lots of communication, I enjoy synchronous interactions where possible. I can deal with ambiguity well and thrive when I have multiple tasks that I am working on. -->

#### Feedback

- [ ] I prefer verbal feedback 
- [ ] I prefer written feedback
- [x] I prefer verbal feedback, followed up in writing 
- [ ] I prefer written feedback, followed up with a verbal conversation

#### Are there any accomodations that can be made to help with you working environment 

<!-- If there are any reasonable accomodations that can be made that you are already aware of that can improve your work environment you are free to put them below. -->

Please contact me through Slack messages or GitLab comments by @ mentioning me, so I can get pinged and respond to the message when I'm able to assist.
